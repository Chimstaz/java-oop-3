/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.Task3.HireStrategy;

import edu.Task3.Employee;
import edu.Task3.Manager;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tom
 */
public class HireFixedSlotsTest {
    

    /**
     * Test of CanHire method, of class HireFixedSlots.
     */
    @Test
    public void testCanHire() {
        System.out.println("CanHire");
        //given
        HireFixedSlots testStrategy = new HireFixedSlots(1);
        Manager testManager = new Manager(1000, "Zbyszek", testStrategy);
        Employee emp2000 = new Employee(2000, "Mietek");
        Employee emp3000 = new Employee(3000, "Zygmunt");
        
        //when
        boolean result1 = testManager.CanHire(emp2000);
        testManager.Hire(emp2000);
        boolean result2 = testManager.CanHire(emp3000);
        
        //then
        assertEquals(true, result1);
        assertEquals(false, result2);
    }
    
}
