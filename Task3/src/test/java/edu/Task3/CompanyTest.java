/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.Task3;

import edu.Task3.HireStrategy.HireEveryone;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tom
 */
public class CompanyTest {
    
    
    private Manager ceo;
    private Manager man1;
    private Manager man2;
    private Employee emp1;
    private Employee emp2;
    private Employee emp3;
    private Employee emp4;
    
    @Before
    public void setUp() {
        HireEveryone strategy = new HireEveryone();
        ceo = new Manager(1000, "Claus", strategy);
        man1 = new Manager(1200, "Mietek", strategy);
        man2 = new Manager(1000, "Maciek", strategy);
        emp1 = new Employee(1000, "Edmund");
        emp2 = new Employee(1000, "Eugeniusz");
        emp3 = new Employee(1100, "Edward");
        emp4 = new Employee(1100, "Edgar");
        Company.getInstance().HireCEO(ceo);
        
        man1.Hire(emp1);
        man1.Hire(emp2);
        
        man2.Hire(emp3);
        
        Company.getInstance().HireEmployee(man1);
        Company.getInstance().HireEmployee(man2);
        Company.getInstance().HireEmployee(emp4);
    }

    /**
     * Test of GetCEO method, of class Company.
     */
    @Test
    public void testGetCEO() {
        System.out.println("GetCEO");
        Manager result = Company.getInstance().GetCEO();
        assertEquals(ceo, result);
    }

    /**
     * Test of toString method, of class Company.
     */
    @Test
    public void testToString() {
        String result = Company.getInstance().toString();
        System.out.print(result + "\n\n");
        assertTrue(result instanceof String);
    }

    /**
     * Test of iterator method, of class Company.
     */
    @Test
    public void testIterator_0args() {
        System.out.println("iterator");
        //given
        List<Employee> expResult = Arrays.asList(ceo, man1, emp1, emp2, man2, emp3, emp4);
        List<Employee> Result = new ArrayList<>();
        
        //when
        for(Employee emp : Company.getInstance()){
            Result.add(emp);
        }
        
        //then
        assertArrayEquals(expResult.toArray(), Result.toArray());
    }

    /**
     * Test of iterator method, of class Company.
     */
    @Test
    public void testIterator_Predicate() {
        System.out.println("iterator");
        //given
        List<Employee> expResult = Arrays.asList(man1, emp3, emp4);
        List<Employee> Result = new ArrayList<>();
        Predicate<Employee> predicate = p -> p.GetSalary() > 1050;
        
        //when
        for (Iterator<Employee> it = Company.getInstance().iterator(predicate); it.hasNext();) {
            Employee emp = it.next();
            Result.add(emp);
        }
        
        //then
        assertArrayEquals(expResult.toArray(), Result.toArray());
    }
    
}
