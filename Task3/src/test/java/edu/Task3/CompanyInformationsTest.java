/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.Task3;

import edu.Task3.HireStrategy.HireEveryone;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tom
 */
public class CompanyInformationsTest {

    
    private Manager ceo;
    private Manager man1;
    private Manager man2;
    private Employee emp1;
    private Employee emp2;
    private Employee emp3;
    private Employee emp4;
    
    @Before
    public void setUp() {
        HireEveryone strategy = new HireEveryone();
        ceo = new Manager(10000, "Claus", strategy);
        man1 = new Manager(12000, "Mietek", strategy);
        man2 = new Manager(10000, "Maciek", strategy);
        emp1 = new Employee(8000, "Edmund");
        emp2 = new Employee(5000, "Eugeniusz");
        emp3 = new Employee(11000, "Edward");
        emp4 = new Employee(11000, "Edgar");
        Company.getInstance().HireCEO(ceo);
        
        man1.Hire(emp1);
        man1.Hire(emp2);
        
        man2.Hire(emp3);
        
        Company.getInstance().HireEmployee(man1);
        Company.getInstance().HireEmployee(man2);
        Company.getInstance().HireEmployee(emp4);
    }

    /**
     * Test of SumOfSalaries method, of class CompanyInformations.
     */
    @Test
    public void testSumOfSalaries() {
        System.out.println("SumOfSalaries");
        int expResult = 10000 + 12000 + 10000 + 8000 + 5000 + 11000 + 11000;
        int result = CompanyInformations.getInstance().SumOfSalaries();
        assertEquals(expResult, result);
    }

    /**
     * Test of GetEmployeeWithLowestSalary method, of class CompanyInformations.
     */
    @Test
    public void testGetEmployeeWithLowestSalary() {
        System.out.println("GetEmployeeWithLowestSalary");
        Employee expResult = emp2;
        Employee result = CompanyInformations.getInstance().GetEmployeeWithLowestSalary();
        assertEquals(expResult, result);
    }

    /**
     * Test of GetUnsatisfiedEmployees method, of class CompanyInformations.
     */
    @Test
    public void testGetUnsatisfiedEmployees() {
        System.out.println("GetUnsatisfiedEmployees");
        List<Employee> expResult = Arrays.asList(ceo, man2, emp1, emp2);
        List<Employee> result = CompanyInformations.getInstance().GetUnsatisfiedEmployees();
        assertTrue(expResult.containsAll(result));
    }
    
}
