/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.Task3;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tom
 */
public class CompanyInformations {
       
    private static CompanyInformations instance = null;
    protected CompanyInformations() {
       
    }
    public static CompanyInformations getInstance() {
       if(instance == null) {
          instance = new CompanyInformations();
       }
       return instance;
    }
    
    public int SumOfSalaries(){
        int sum = 0;
        for(Employee emp : Company.getInstance()) sum += emp.GetSalary();
        return sum;
    }
    
    public Employee GetEmployeeWithLowestSalary(){
        if(Company.getInstance().GetCEO() == null) return null;
        Employee min = Company.getInstance().GetCEO();
        int minS = min.GetSalary();
        for(Employee emp : Company.getInstance()){
            if(minS > emp.GetSalary()){
                min = emp;
                minS = min.GetSalary();
            }
        }
        return min;
    }
    
    public List<Employee> GetUnsatisfiedEmployees(){
        List<Employee> ret = new ArrayList<>();
        for(Employee emp : Company.getInstance()){
            if(!emp.Satisfaction()){
                ret.add(emp);
            }
        }
        return ret;
    }
    
}
