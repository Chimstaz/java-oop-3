/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.Task3.HireStrategy;

import edu.Task3.Employee;
import edu.Task3.Manager;

/**
 *
 * @author Tom
 */
public class HireFixedSlots implements HireStrategy {

    private final int Slots;
    
    public HireFixedSlots(int Slots){
        this.Slots = Slots;
    }
    
    @Override
    public boolean CanHire(Manager man, Employee emp) {
        return Slots > man.NumberOfEmployees();
    }
    
}
