/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.Task3;

import java.util.Iterator;
import java.util.function.Predicate;

/**
 *
 * @author Tom
 */
public class Company implements Iterable<Employee>{
   private static Company instance = null;
   private Manager CEO = null;
   protected Company() {
       
   }
   public static Company getInstance() {
      if(instance == null) {
         instance = new Company();
      }
      return instance;
   }
   
   public void HireCEO(Manager CEO){
       this.CEO = CEO;
   }
   
   public void HireEmployee(Employee emp){
       if(CEO.CanHire(emp)){
           CEO.Hire(emp);
       }
   }
   
   public Manager GetCEO(){
       return CEO;
   }
   
   @Override
   public String toString(){
       return CEO.PrintAsCEO();
   }

    @Override
    public Iterator<Employee> iterator() {
        return new CompanyIterator<>();
    }
    
    public Iterator<Employee> iterator(Predicate<Employee> predicate){
        return new CompanyIterator<>(predicate);
    }
}
