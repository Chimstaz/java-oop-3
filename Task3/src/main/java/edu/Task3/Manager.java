/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.Task3;

import edu.Task3.HireStrategy.HireStrategy;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tom
 */
public class Manager extends Employee {
    
    private final List<Employee> Employees = new ArrayList<>();
    private final HireStrategy MyHireStrategy;
    
    public Manager(int Salary, String Name, HireStrategy toUse) {
        super(Salary, Name);
        MyHireStrategy = toUse;
    }
    
    @Override
    public String GetName(){
        return Name + " - Manager";
    }
    
    public int NumberOfEmployees(){
        return Employees.size();
    }
    
    public int SumOfEmployeesSalaries(){
        int sum = 0;
        for(Employee emp : Employees){
            sum += emp.GetSalary();
        }
        return sum;
    }
    
    public boolean CanHire(Employee emp){
        return MyHireStrategy.CanHire(this, emp);
    }
    
    public void Hire(Employee emp){
        if(!CanHire(emp)){
            throw new IllegalArgumentException(emp.GetName() + " can't be hired by " + this.GetName());
        }
        else{
            Employees.add(emp);
        }
    }
    
    @Override
    public String CompanyPrintList(String Column){
        StringBuilder out = new StringBuilder(Column + Name + " - Manager");
        Column += "\t";
        for(Employee emp : Employees) out.append("\n").append(emp.CompanyPrintList(Column));
        return out.toString();
    }
    
    public String PrintAsCEO(){
        StringBuilder out = new StringBuilder(Name + " - CEO");
        String Column = "\t";
        for(Employee emp : Employees) out.append("\n").append(emp.CompanyPrintList(Column));
        return out.toString();
    }
    
    public Employee GetEmployee(int i){
        if(i < Employees.size() && i >= 0){
            return Employees.get(i);
        }
        else{ 
            return null;
        }
    }
}
