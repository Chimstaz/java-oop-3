/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.Task3;

/**
 *
 * @author Tom
 */
public class Employee {
    protected int Salary;
    protected String Name;
    
    public Employee(int Salary, String Name){
        this.Salary = Salary;
        this.Name = Name;
    }
    
    public String GetName(){
        return Name + " - Employee";
    }
    
    public int GetSalary(){
        return Salary;
    }
    
    public boolean Satisfaction(){
        return Salary > 10000;
    }
    
    public String CompanyPrintList(String Column){
        return Column + Name + " - Employee";
    }
    
}
