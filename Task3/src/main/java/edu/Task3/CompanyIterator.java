/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.Task3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

/**
 *
 * @author Tom
 */
class CompanyIterator<Employee> implements Iterator<Employee>{

    class ManagersEmployee{
        public Manager manager;
        public int indexOfEmployee;
        
        public ManagersEmployee(Manager manager, int index){
            this.manager = manager;
            this.indexOfEmployee = index;
        }
    }
    
    private final List<ManagersEmployee> Hierarchy;
    private final Predicate<Employee> predicate;
    
    
    public CompanyIterator() {
        Hierarchy = new ArrayList<>();
        this.predicate = p -> true;
    }
    
    public CompanyIterator(Predicate<Employee> predicate) {
        Hierarchy = new ArrayList<>();
        this.predicate = predicate;
    }

    @Override
    public boolean hasNext() {
        if(Hierarchy.isEmpty()){
            if(Company.getInstance().GetCEO() == null){
                return false;
            }
            if(predicate.test((Employee)Company.getInstance().GetCEO())){
                return true;
            }
            Hierarchy.add(new ManagersEmployee(Company.getInstance().GetCEO(), -1));
        }
        int lastIndex = Hierarchy.size()-1;
        Employee Current;
        int i = 1;  //wich employee of deppest in hierarchy menager to get
        while((Current = (Employee) Hierarchy.get(lastIndex).manager.GetEmployee( Hierarchy.get(lastIndex).indexOfEmployee + i))== null 
                || !predicate.test(Current)){
            if(Current == null){
                i = 1;
                lastIndex--;
                if(lastIndex < 0) return false;
            }
            else{
                if(Current instanceof Manager){
                    Hierarchy.get(lastIndex).indexOfEmployee += i;
                    lastIndex++;
                    Hierarchy.add(new ManagersEmployee((Manager) Current, -1));
                    i = 1;
                }
                i++;
            }
        }
        return true;
    }

    @Override
    public Employee next() {
        if(Hierarchy.isEmpty()){
            Hierarchy.add(new ManagersEmployee(Company.getInstance().GetCEO(), -1));
            return (Employee) Company.getInstance().GetCEO();
        }
        int lastIndex = Hierarchy.size()-1;
        Employee Current;
        Hierarchy.get(lastIndex).indexOfEmployee++;
        while((Current = (Employee) Hierarchy.get(lastIndex).manager.GetEmployee( Hierarchy.get(lastIndex).indexOfEmployee ))== null
                || !predicate.test(Current)){
            if(Current == null){
                Hierarchy.remove(lastIndex);
                lastIndex--;
                Hierarchy.get(lastIndex).indexOfEmployee++;
            }
            else{
             Hierarchy.get(lastIndex).indexOfEmployee++;  
            }
        }
        if(Current instanceof Manager){
            Hierarchy.add(new ManagersEmployee((Manager) Current, -1));
        }
        return Current;
    }
    
}
